from myhdl import block, instance, Signal, instances, delay, intbv, ResetSignal, StopSimulation
from axis import Axis
from clk_stim import clk_stim
import os

from overlap_save import sync_overlap_save

# # sample data from youtube clip above
# h = [1, 2, 3]
# x = [1, 2, -1, 2, 3, -2, -3, -1, 1, 1, 2, -1]
x = [90, 55, 83, 64, 18, 80, 82, 9, 24, 54, 16, 47, 46, 4, 2, 33, 43, 3, 44, 11, 49, 61, 91, 38, 35, 40, 95, 81]
h = [intbv(1, min=0, max=2), intbv(2, min=0, max=3), intbv(-1, min=-1, max=0)]
# x = [intbv(2, min=0, max=3), intbv(3, min=0, max=4), intbv(-2, min=-2, max=0), intbv(-3, min=-3, max=0),
#      intbv(-1, min=-1, max=0), intbv(1, min=0, max=2), intbv(1, min=0, max=2), intbv(2, min=0, max=3),
#      intbv(-1, min=-1, max=0)]

@block
def testbench(vhdl_output_path=None):

    reset = ResetSignal(0, active=0, async=False)
    clk = Signal(bool(0))

    axis_input = Axis(32)
    axis_output = Axis(32)

    clk_gen = clk_stim(clk, period=10)

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(54)
        yield clk.negedge
        reset.next = 1

    @instance
    def write_stim():
        i = 0
        yield reset.posedge
        while i < len(x):
            yield clk.negedge
            axis_input.tvalid.next = 1
            axis_input.tdata.next = x[i]
            if i == len(x) - 1:
                axis_input.tlast.next = 1
            else:
                axis_input.tlast.next = 0
            if axis_input.tready == 1:
                i += 1
        yield clk.negedge
        axis_input.tvalid.next = 0

    @instance
    def read_stim():
        yield reset.posedge
        yield clk.negedge
        axis_output.tready.next = 1
        while True:
            yield clk.negedge
            rx = axis_output.tdata
            if axis_output.tlast == 1:
                break

        for i in range(10):
            yield clk.negedge
        raise StopSimulation()

    uut = sync_overlap_save(clk, reset, 3, axis_input, axis_output)

    if vhdl_output_path is not None:
        uut.convert(hdl='VHDL', path=vhdl_output_path)
    return instances()


if __name__ == '__main__':
    trace_save_path = '../out/testbench/'
    vhdl_output_path = '../out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    tb = testbench(vhdl_output_path)
    tb.config_sim(trace=True, directory=trace_save_path, name='overlap_save_tb')
    tb.run_sim()

#
# M = len(h)      # step 1
# L = 2           # step 2
# N = L + M - 1   # step 3
#
# y = [0] * len(x)    # inicjalizacja y
# xz = [0] * (M-1)    # dodaj zera na początku
# xz += x
#
# xz += [0] * (len(x) % L)
# xs = [0] * L
# for k in range(int(len(xz)/L)+1):
#     xs = xz[k * L:(k+1) * L+M-1]
#     ys = circular_convolution(xs, h)    # step 4
#     # ys = ys[M:]
#     y[(k*L):(k+1)*L] = ys[(M-1):]
# print(y)



