from myhdl import block, always_seq, instance, instances, intbv, enum, delay, Signal
'''
http://gdr.geekhood.net/gdrwpl/heavy/studia/Lecture2007%238__Splot_Korelacja.pdf
https://www.youtube.com/watch?v=AsVX2CxviWI&t=436s
https://media.readthedocs.org/pdf/fpga-designs-with-myhdl/latest/fpga-designs-with-myhdl.pdf?fbclid=IwAR0iObNsoJZWq4444kiXFHCI6lCpYQA8-XFJZb1OvATK0Pnwirvi0jFljr8
http://blog.robertelder.org/overlap-add-overlap-save/
'''

# ---------------------------------------------------------------------------------------------------
# The valid and ready signals allow for handshake process to occur for each channel on AXI interface.
# For transmitting any signal (address/data/response/etc) the relevant channel source provides an
# active valid signal and the same channel’s destination must provide an active ready signal.
# After both signals are active, transmission may occur on that channel
# ---------------------------------------------------------------------------------------------------

@block
def sync_overlap_save(clk, reset, segment_length, axis_data, axis_output):
    """
    Hardware block defining overlap save processing behaviour (synchronous).
    :param clk: clock signal source
    :param reset: asynchronous reset signal
    :param segment_length: data segment length
    :param axis_data: input data signal
    :param axis_output output data signal
    :return: overlap save asynchronous processing block
    """
    # define block processing states
    state_t = enum('COLLECT_FIR', 'COLLECT_DATA', 'COLLECT_MISSING_DATA', 'EMIT_OUTPUT_BUFFER', 'IDLE')
    # filter impulse response is dedicated to the block
    h = [Signal(intbv(1, min=0, max=2)[8:]), Signal(intbv(2, min=0, max=3)[8:]), Signal(intbv(3, min=2, max=4)[8:])]
    # overlap data length
    overlap_length = Signal(intbv(len(h) - 1)[32:])
    # data segment counter
    buffer_counter = Signal(intbv(0, min=0, max=segment_length)[32:])
    # block state signal
    state = Signal(state_t.COLLECT_FIR)
    # data segment buffer
    data_buffer = [Signal(intbv(0)[32:]) for _ in range(segment_length)]
    # overlap data buffer
    overlap_buffer = [Signal(intbv(0)[32:]) for _ in range(overlap_length)]
    # zero-padded filter impulse response signal
    padded_fir = [Signal(intbv(0)[32:0]) for _ in range(segment_length + overlap_length)]
    # circular convolution input data signal
    data_sequence = [Signal(intbv(0)[32:]) for _ in range(segment_length + overlap_length)]
    # circular convolution output data signal
    cconv_output_buffer = [Signal(intbv(0)[32:]) for _ in range(segment_length + overlap_length)]
    # output data buffer
    output_buffer = [Signal(intbv(0)[32:]) for _ in range(segment_length)]

    @always_seq(clk.posedge, reset=reset)
    def process():
        if state == state_t.COLLECT_FIR:
            for i in range(len(h)):
                padded_fir[i] = intbv(int(h[i]))[32:]
            # change hardware block state
            state.next = state_t.COLLECT_DATA
            axis_data.tready.next = 1
            axis_output.tlast.next = 0
        elif state == state_t.COLLECT_DATA:
            # check if channel source valid signal is active(set)
            if axis_data.tvalid == 1:
                if axis_data.tlast:
                    state.next = state_t.COLLECT_MISSING_DATA
                    if axis_output.tready:
                        # exchange data
                        data_buffer[int(buffer_counter.val)] = intbv(axis_data.tdata.val)[32:]
                        axis_output.tdata.next = output_buffer[int(buffer_counter.val)]
                        if buffer_counter == segment_length - 1:
                            axis_output.tvalid.next = 1
                            # clear data buffer counter for the next stage processing
                            buffer_counter.next = 0
                            # clear processing output buffer
                            for i in range(len(cconv_output_buffer)):
                                cconv_output_buffer[i] = 0
                            # concatenate overlap and data buffers
                            for i in range(overlap_length):
                                data_sequence[i] = intbv(int(overlap_buffer[i]))[32:]
                            for i in range(segment_length):
                                data_sequence[overlap_length + i] = intbv(int(data_buffer[i]))[32:]
                                # perform circular convolution on concatenated input
                            for i in range(segment_length + overlap_length):
                                for j in range(segment_length + overlap_length):
                                    cconv_output_buffer[i] = cconv_output_buffer[i] + data_sequence[j] * \
                                                             padded_fir[(i - j) % (segment_length + overlap_length)]
                            for i in range(segment_length):
                                output_buffer[i] = cconv_output_buffer[overlap_length + i]
                            # save probes from data buffer to overlap buffer for the next processing
                            for i in range(overlap_length):
                                overlap_buffer[i] = intbv(int(data_buffer[segment_length - overlap_length + i]))[32:]
                        else:
                            buffer_counter.next[:] = buffer_counter + 1
                    else:
                        axis_output.tvalid.next = 0
                else:
                    if axis_output.tready:
                        # exchange data
                        axis_output.tvalid.next = 1
                        data_buffer[int(buffer_counter.val)] = intbv(axis_data.tdata.val)[32:]
                        axis_output.tdata.next = output_buffer[int(buffer_counter)]
                        if buffer_counter == segment_length - 1:
                            # clear data buffer counter for the next stage processing
                            buffer_counter.next = 0
                            # clear processing output buffer
                            for i in range(len(cconv_output_buffer)):
                                cconv_output_buffer[i] = 0
                            # concatenate overlap and data buffers
                            for i in range(overlap_length):
                                data_sequence[i] = intbv(int(overlap_buffer[i]))[32:]
                            for i in range(segment_length):
                                data_sequence[overlap_length + i] = int(data_buffer[i])
                                # perform circular convolution on concatenated input
                            for i in range(segment_length + overlap_length):
                                for j in range(segment_length + overlap_length):
                                    cconv_output_buffer[i] = cconv_output_buffer[i] + data_sequence[j] * \
                                                             padded_fir[(i - j) % (segment_length + overlap_length)]
                            for i in range(segment_length):
                                output_buffer[i] = cconv_output_buffer[overlap_length + i]
                            # save probes from data buffer to overlap buffer for the next processing
                            for i in range(overlap_length):
                                overlap_buffer[i] = intbv(int(data_buffer[segment_length - overlap_length + i]))[32:]
                        else:
                            buffer_counter.next[:] = buffer_counter + 1
                    else:
                        axis_output.tvalid.next = 0
            else:
                axis_output.tvalid.next = 0
        elif state == state_t.COLLECT_MISSING_DATA:
            if axis_output.tready:
                # exchange data
                data_buffer[int(buffer_counter.val)] = intbv(0)[32:]
                axis_output.tdata.next = output_buffer[int(buffer_counter.val)]
                axis_output.tvalid.next = 1
                if buffer_counter == segment_length - 1:
                    # clear data buffer counter for the next stage processing
                    buffer_counter.next = 0
                    # clear processing output buffer
                    for i in range(len(cconv_output_buffer)):
                        cconv_output_buffer[i] = 0
                    # concatenate overlap and data buffers
                    for i in range(overlap_length):
                        data_sequence[i] = intbv(int(overlap_buffer[i]))[32:]
                    for i in range(segment_length):
                        data_sequence[overlap_length + i] = intbv(int(data_buffer[i]))[32:]
                        # perform circular convolution on concatenated input
                    for i in range(segment_length + overlap_length):
                        for j in range(segment_length + overlap_length):
                            cconv_output_buffer[i] = cconv_output_buffer[i] + data_sequence[j] * \
                                                     padded_fir[(i - j) % (segment_length + overlap_length)]
                    for i in range(segment_length):
                        output_buffer[i] = cconv_output_buffer[overlap_length + i]
                    # reset overlap buffer
                    for i in range(overlap_length):
                        overlap_buffer[i] = 0
                    state.next = state_t.EMIT_OUTPUT_BUFFER
                else:
                    buffer_counter.next[:] = buffer_counter + 1
            else:
                axis_output.tvalid.next = 0
        elif state == state_t.EMIT_OUTPUT_BUFFER:
            if axis_output.tready:
                data_buffer[int(buffer_counter)] = 0
                axis_output.tdata.next = output_buffer[int(buffer_counter)]
                axis_output.tvalid.next = 1
                if buffer_counter == segment_length - 1:
                    axis_output.tlast.next = 1
                    buffer_counter.next[:] = 0
                    state.next = state_t.IDLE
                else:
                    buffer_counter.next[:] = buffer_counter + 1
            else:
                axis_output.tvalid.next = 0
        elif state == state_t.IDLE:
            axis_output.tdata.next = 0
            axis_output.tvalid.next = 0
        else:
            raise ValueError("Undefined state")
    return instances()
